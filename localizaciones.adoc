== Posibles localizaciones

// Describir lugar y poner alguna foto. ¡Ojo!

=== Rivera del Huéznar

Bonito lugar en la Sierra Norte de Sevilla.

.Ribera del río Huéznar a su paso por La Fundición. https://commons.wikimedia.org/wiki/File:Ribera_del_Hueznar_01.jpg[Créditos].
image::images/576px-Ribera_del_Hueznar_01.jpg[Ribera del Huéznar]

=== Orilla de la presa de la albuera

Una buena ubicación para pasar una estupenda tarde de barbacoa. Al frescor de la brisa del lago.

.La albuera de Zafra. https://www.flickr.com/photos/49651443@N06/6708981155[Créditos]
image::images/zafra.jpg[Albuera de zafra]

=== Pinares de la Puebla

Perfecto para pasar un buen día de barbacoa donde incluyen merenderos bajo la sombra de los pinares.

.Pinares de la Puebla del Río y Aznalcázar. http://www.caminosvivos.com/recurso-detalle/3596/pinares-de-la-puebla-del-rio-y-aznalcazar[Créditos].
image::images/Pinares_Puebla.jpg[Pinares de la Puebla]

=== Estepa 

Si te gustan los mantecados y las hojaldrinas este es el lugar perfecto para ti. Ademas tiene unas vista espectaculares

.Estepa. https://sevilla.abc.es/Media/201303/16/estepa-nevada--644x362--644x362.jpg
image::images/estepa.jpg[Estepa]

=== Orilla del río Guadiana

A la sombra del castillo de Medellín, el río Guadiana es el perfecto lugar para una barbacoa (o caldereta,
como se dice de toda la vida en mi pueblo). Se puede pescar a la par que observar un desastre ecológico como
es la plaga del camalote, un nenúfar invasor que ha alterado el ecosistema del río.

.Castillo de Medellín junto al puente romano, a orillas del río Guadiana. https://2.bp.blogspot.com/-WC0UMBQeMsg/Vr5LNJazyiI/AAAAAAAAUFQ/BSvXes2YArQ/s1600/Medelli%25CC%2581n%252CPuente%2Bde%2BLos%2BAustrias.JPG[Créditos].
image::images/medellin.jpg[Castillo de Medellín, puente romano y río Guadiana]

